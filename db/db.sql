CREATE DATABASE IF NOT EXISTS api_user_nodejs;

USE api_user_nodejs;

CREATE TABLE `api_user_nodejs`.`employees` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `salary` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

DESCRIBE employees;

INSERT INTO `api_user_nodejs`.`employees` (`name`, `salary`) VALUES ('juan', '800');
INSERT INTO `api_user_nodejs`.`employees` (`name`, `salary`) VALUES ('daniel', '0');
INSERT INTO `api_user_nodejs`.`employees` (`name`, `salary`) VALUES ('zube', '400');
INSERT INTO `api_user_nodejs`.`employees` (`name`, `salary`) VALUES ('maria', '550');



USE `api_user_nodejs`;
DROP procedure IF EXISTS `employeeAddOrEdit`;

DELIMITER $$
USE `api_user_nodejs`$$
CREATE PROCEDURE `employeeAddOrEdit` (
	IN _id INT,
    IN _name VARCHAR(255),
    IN _salary INT
)
BEGIN
	IF _id = 0 THEN
		INSERT INTO employees (name, salary) VALUES (_name, _salary);
	SET _id = last_insert_id();
    ELSE
		UPDATE employees SET
        name = _name,
        salary = _salary
        WHERE id = _id;
	END IF;

    SELECT _id AS id;
END$$

DELIMITER ;

