const controller = {};
const mysqlConnection = require('../database');

controller.index = (req, res) => {
    mysqlConnection.query('SELECT * FROM employees', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
};

controller.edit = (req, res) => {
    const {id} = req.params;

    mysqlConnection.query('SELECT * FROM employees WHERE id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json(rows[0]);
        } else {
            console.log(err);
        }
    });
};

controller.store = (req, res) => {
    let {id, name, salary} = req.body;
    const query = `CALL employeeAddOrEdit(?, ?, ?);`;
    mysqlConnection.query(query, [id, name, salary], (err, rows, fields) => {
        if (!err) {
            res.json({state: 'Employee saved'});
        } else {
            console.log(err);
        }
    });
};

controller.update = (req, res) => {
    const {id} = req.params;
    let {name, salary} = req.body;
    const query = `CALL employeeAddOrEdit(?, ?, ?);`;
    mysqlConnection.query(query, [id, name, salary], (err, rows, fields) => {
        if (!err) {
            res.json({state: 'Employee updated'});
        } else {
            console.log(err);
        }
    });
};

controller.destroy = (req, res) => {
    const {id} = req.params;
    mysqlConnection.query('DELETE FROM employees where id = ?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({state: 'Employee deleted'});
        } else {
            console.log(err);
        }
    });
};

module.exports = controller;