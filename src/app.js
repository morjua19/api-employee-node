const express = require('express');
const mysql = require('mysql');
const app = express();

// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(express.json());

// Importing routes
const employeesRoutes = require('./routes/empoyees');
// Routes
app.use(employeesRoutes);

// Starting the server
app.listen(app.get('port'), () => {
    console.log('Server on port ', app.get('port'));
});