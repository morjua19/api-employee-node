const express = require('express');
const router = express.Router();
const EmployeController = require('../Controllers/EmployeController');


router.get('/', EmployeController.index);

router.get('/:id', EmployeController.edit);

router.post('/', EmployeController.store);

router.put('/:id', EmployeController.update);

router.delete('/:id', EmployeController.destroy);

module.exports = router;